# -*- coding: utf-8 -*-


# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Copyright (C) 2019 Federacion Galega de Natación (FEGAN) http://www.fegan.org
# Author: Daniel Muñiz Fontoira (2017) <dani@damufo.com>


"""
A partir dunha base de datos Meet M. xera unha inscricións para ser
importadas no Xesde. Isto permite pasar as inscricións de Meet M. a
Piscina.
"""


import os
import sys

# PACKAGE_PARENT = '..'
# SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
# sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

# from database_mdb_win import DatabaseMdbWin
# import win32com.client
from classes.database import Database
from classes.date_time import str2datetime, datetime2str, time2int, str2date, date2str
from classes.files import set_file_content


class Mm2Xes(object):
    """ sese = separa sedes"""

    def __init__(self, platform, dbs_path):
        self.dbs = Database(platform=platform)
        self.dbs.connect(dbs_path)
        self.file_path = os.path.splitext(dbs_path)[0]

    def gen_insc(self, file_path):

        sql_clubes = ''' select clubid, code, shortname from club '''
        res = self.dbs.exec_sql(sql=sql_clubes)
        clubes = {}
        (CLUB_ID, CODE, NAME) = range(3)
        clubes = {}
        for i in res:
            clubes[i[CLUB_ID]] = {
                'rfen_id': i[CODE],
                'name':  i[NAME]
                }

        sql_persons = '''
Select athleteid, birthdate, clubid, firstname, lastname, gender, license
from athlete '''
        res = self.dbs.exec_sql(sql=sql_persons)
        (PERSON_ID, BIRTH_DATE, CLUB_ID, NAME, SURNAME, GENDER_ID,
         LICENSE_ID) = range(7)
        persons = {}
        for i in res:
            persons[i[PERSON_ID]] = {
                'birth_date': i[BIRTH_DATE],
                'club_id':  clubes[i[CLUB_ID]]['rfen_id'],
                'name':  i[NAME],
                'surname':  i[SURNAME],
                'gender_id':  i[GENDER_ID],
                'license_id':  i[LICENSE_ID],
                }
        sql_styles = '''
select swimstyleid, distance, relaycount, stroke from swimstyle '''
        res = self.dbs.exec_sql(sql=sql_styles)
        (STYLE_ID, DISTANCE, MEMBERS, STYLE) = range(4)
        styles = {}
        initial_letter = {'1': 'L', '2': 'E', '3': 'B', '4': 'M', '5': 'S'}
        for i in res:
            styles[i[STYLE_ID]] = {
                'distance': i[DISTANCE],
                'members': i[MEMBERS],
                'style': initial_letter[i[STYLE]],
            }

        sql_events = '''
select swimeventid, swimstyleid, gender from swimevent '''
        res = self.dbs.exec_sql(sql=sql_events)
        (EVENT_ID, STYLE_ID, GENDER_ID) = range(3)
        events = {}
        genders = {'1': 'M', '2': 'F', '3': 'X'}
        for i in res:
            if i[STYLE_ID]:
                events[i[EVENT_ID]] = {
                    'distance': styles[i[STYLE_ID]]['distance'],
                    'members': styles[i[STYLE_ID]]['members'],
                    'style_id': styles[i[STYLE_ID]]['style'],
                    'gender_id': genders[i[GENDER_ID]],
                }

        sql_categories = '''
select agegroupid, code from agegroup '''
        res = self.dbs.exec_sql(sql=sql_categories)
        (CATEGORY_ID, NAME) = range(2)
        categories = {}
        for i in res:
            categories[i[CATEGORY_ID]] = i[CODE]

        def date_formated(date_text):
            value = ''
            if len(date_text) > 8:
                date_m = date_text[0:2]
                date_d = date_text[3:5]
                date_y = date_text[6:8]
                if int(date_y) > 19:
                    year_p1 = '19'
                else:
                    year_p1 = '20'
                value = year_p1 + date_y + date_m + date_d

            return value

        sql_inscriptions = '''
Select athleteid, agegroupid, qtcity, qtdate, swimeventid, entrytime, entrycourse
from swimresult '''
        res = self.dbs.exec_sql(sql=sql_inscriptions)
        (PERSON_ID, CATEGORY_ID, VENUE, DATE, EVENT_ID, TIME,
         POOL_CHRONO) = range(7)
        inscriptions = {}
        lines = []
        lines.append((
            'champ_id',
            'event_id',
            'gender_id',
            'category_id',
            'person_id',
            'club_id',
            'pool_id',
            'chrono_id',
            'mark_hundredth',
            'equated_hundredth',
            'xdate',
            'venue',
            'inscribed',
            'surname',
            'name',
            'birth_date',
            'type_id',
            'member_event_id',
            'member_gender_id',
            'member_person_id'))
        for i in res:
            if events[i[EVENT_ID]]['members'] == '1':
                event_id = '{}{}'.format(
                    events[i[EVENT_ID]]['distance'],
                    events[i[EVENT_ID]]['style_id'])
            else:
                event_id = '{}X{}{}'.format(
                    events[i[EVENT_ID]]['members'],
                    events[i[EVENT_ID]]['distance'],
                    events[i[EVENT_ID]]['style_id'])
            if i[POOL_CHRONO] == '1':
                pool_id = '50'
                chrono_id = 'E'
            elif i[POOL_CHRONO] == '2':
                pool_id = '25'
                chrono_id = 'E'
            elif i[POOL_CHRONO] == '257':
                pool_id = '50'
                chrono_id = 'M'
            elif i[POOL_CHRONO] == '258':
                pool_id = '25'
                chrono_id = 'M'
            inscriptions[i[PERSON_ID]] = {
                'champ_id': '',
                'event_id': event_id,
                'gender_id': events[i[EVENT_ID]]['gender_id'],
                'category_id': i[CATEGORY_ID],
                'person_id': persons[i[PERSON_ID]]['license_id'],
                'club_id':  persons[i[PERSON_ID]]['club_id'],
                'pool_id': pool_id,
                'chrono_id': chrono_id,
                'mark_hundredth': i[TIME],
                'equated_hundredth': i[TIME],
                'xdate': date_formated(i[DATE]),
                'venue': i[VENUE],
                'inscribed': 1,
                'surname':  persons[i[PERSON_ID]]['surname'],
                'name':  persons[i[PERSON_ID]]['name'],
                'birth_date':'',
                'type_id': 'I',
                'member_event_id':  '',
                'member_gender_id':  '',
                'member_person_id':  '',
                }
            

            lines.append((
                '11819F0111CMV',
                event_id,
                events[i[EVENT_ID]]['gender_id'],
                'ABSO',
                persons[i[PERSON_ID]]['license_id'].zfill(9),
                persons[i[PERSON_ID]]['club_id'],
                pool_id,
                chrono_id,
                i[TIME][:-1],
                i[TIME][:-1],
                date_formated(i[DATE]),
                i[VENUE],
                '1',
                persons[i[PERSON_ID]]['surname'],
                persons[i[PERSON_ID]]['name'],
                date_formated(persons[i[PERSON_ID]]['birth_date']),
                'I',
                '',
                '',
                ''))

        sql_relays = '''
Select relayid, clubid, agegroupid, qtcity, qtdate, swimeventid, entrytime, entrycourse
from relay '''
        res = self.dbs.exec_sql(sql=sql_relays)
        (RELAY_ID, CLUB_ID, CATEGORY_ID, VENUE, DATE, EVENT_ID, TIME,
         POOL_CHRONO) = range(8)
        inscriptions = {}
        # lines = []
        for i in res:
            # print(i)
            if events[i[EVENT_ID]]['members'] == '1':
                event_id = '{}{}'.format(
                    events[i[EVENT_ID]]['distance'],
                    events[i[EVENT_ID]]['style_id'])
            else:
                event_id = '{}X{}{}'.format(
                    events[i[EVENT_ID]]['members'],
                    events[i[EVENT_ID]]['distance'],
                    events[i[EVENT_ID]]['style_id'])
            if i[POOL_CHRONO] == '1':
                pool_id = '50'
                chrono_id = 'E'
            elif i[POOL_CHRONO] == '2':
                pool_id = '25'
                chrono_id = 'E'
            elif i[POOL_CHRONO] == '257':
                pool_id = '50'
                chrono_id = 'M'
            elif i[POOL_CHRONO] == '258':
                pool_id = '25'
                chrono_id = 'M'
            

            lines.append((
                '11819F0111CMV',
                event_id,
                events[i[EVENT_ID]]['gender_id'],
                'ABSO',
                i[RELAY_ID],
                clubes[i[CLUB_ID]]['rfen_id'],
                pool_id,
                chrono_id,
                i[TIME][:-1],
                i[TIME][:-1],
                date_formated(i[DATE]),
                i[VENUE],
                '1',
                '{} {}'.format(clubes[i[CLUB_ID]]['name'], categories[i[CATEGORY_ID]]),
                categories[i[CATEGORY_ID]],
                '',
                'R',
                '',
                '',
                ''))

        with open(file_path, 'w') as outfile:
            for i in lines:
                line = '#'.join(i)
                outfile.write(line + '\n')

        # sql_champ = ''' select swimresultid from swimresult '''
        # res = self.dbs.exec_sql(sql=sql_champ)
        # for i in res:

