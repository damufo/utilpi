# -*- coding: utf-8 -*-


# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Copyright (C) 2017 Federacion Galega de Natación (FEGAN) http://www.fegan.org
# Author: Daniel Muñiz Fontoira (2017) <dani@damufo.com>


'''
Xera un fichero LEV a partir da base de datos de Piscina.
'''

import os
from classes.database import Database
from classes.date_time import str2datetime, datetime2str, time2int
from classes.files import set_file_content


# Licenses to fix
'''
Example:
dni_levid_replaces = {
"58439636": "3515420",
"54127422": "3519230",
} 
'''
dni_levid_replaces = {}

class Mdb2Lev(object):
    """ Export Piscina results to LEV format"""

    def __init__(self, platform, dbs_path):
        self.dbs = Database(platform=platform)
        self.dbs.connect(dbs_path)
        self.file_path = os.path.splitext(dbs_path)[0]

    def gen_lev(self):
        """
        Generate file export lev format
        """

        cat_lev = {}
        for i in (
                ("9 A", "9 años"),
                ("10 A", "10 años"),
                ("11 A", "11 años"),
                ("12 A", "12 años"),
                ("13 A", "13 años"),
                ("14 A", "14 años"),
                ("15 A", "15 años"),
                ("16 A", "16 años"),
                ("17 A", "17 años"),
                ("18 A", "18 años"),
                ("20+", "20+"),
                ("+80", "+80"),
                ("+100", "+100"),
                ("+120", "+120"),
                ("+160", "+160"),
                ("+200", "+200"),
                ("+240", "+240"),
                ("+280", "+280"),
                ("+320", "+320"),
                ("95+", "95+"),
                ("90+", "90+"),
                ("85+", "85+"),
                ("80+", "80+"),
                ("75+", "75+"),
                ("70+", "70+"),
                ("65+", "65+"),
                ("60+", "60+"),
                ("55+", "55+"),
                ("50+", "50+"),
                ("45+", "45+"),
                ("40+", "40+"),
                ("35+", "35+"),
                ("30+", "30+"),
                ("25+", "25+"),
                ("ABSO.X", "Absoluto Mixto"),
                ("ABSO.F", "Absoluto Femenino"),
                ("ABSO.M", "Absoluto Masculino"),
                ("ABSJ.X", "Absouto Joven Mixto"),
                ("ABSJ.F", "Absoluto Joven Femenino"),
                ("ABSJ.M", "Absoluto Joven Masculino"),
                ("JUNI.X", "Junior Mixto"),
                ("JUNI.F", "Junior Femenino"),
                ("JUNI.M", "Junior Masculino"),
                ("INFA.X", "Infantil Mixto"),
                ("INFA.F", "Infantil Femenino"),
                ("INFA.M", "Infantil Masculino"),
                ("ALEV.X", "Alevín Mixto"),
                ("ALEV.F", "Alevín Femenino"),
                ("ALEV.M", "Alevín Masculino"),
                ("BENJ.X", "Benjamín Mixto"),
                ("BENJ.F", "Benjamín Femenino"),
                ("BENJ.M", "Benjamín Masculino"),
                ("PROM.X", "Prebenjamín Mixto"),
                ("PROM.F", "Prebenjamín Femenino"),
                ("PROM.M", "Prebenjamín Masculino"),):
            cat_lev[i[0].upper()] = i[1]

        lines = ['''phaseresult.resultable.@id#
phaseresult.last_name#
phaseresult.first_name#
phaseresult.resultable.@where:code#
phaseresult.resultable.@type#
phaseresult.style.name#
phaseresult.value#
phaseresult.goal#
phaseresult.participants_amount#
phaseresult.date#
phaseresult.category.maximum_age#
phaseresult.category.minimum_age#
phaseresult.category.name#
phaseresult.category.@where:categorydisciplines.discipline.id#
phaseresult.competition#
phaseresult.location#
phaseresult.discipline_fields.pool_size#
phaseresult.discipline_fields.chronometer#
phaseresult.custom_fields.pool_name#
phaseresult.gender#
phaseresult.custom_fields.event_number#
phaseresult.custom_fields.event_round#
phaseresult.official#
phaseresult.office.name#
phaseresult.phaseresult.resultable.@id#
phaseresult.phaseresult.last_name#
phaseresult.phaseresult.first_name#
phaseresult.phaseresult.resultable.@where:code#
phaseresult.phaseresult.resultable.@type#
phaseresult.phaseresult.style.name#
phaseresult.phaseresult.value#
phaseresult.phaseresult.goal#
phaseresult.phaseresult.participants_amount#
phaseresult.phaseresult.date#
phaseresult.phaseresult.category.maximum_age#
phaseresult.phaseresult.category.minimum_age#
phaseresult.phaseresult.category.name#
phaseresult.phaseresult.category.@where:categorydisciplines.discipline.id#
phaseresult.phaseresult.competition#
phaseresult.phaseresult.location#
phaseresult.phaseresult.discipline_fields.pool_size#
phaseresult.phaseresult.discipline_fields.chronometer#
phaseresult.phaseresult.custom_fields.pool_name#
phaseresult.phaseresult.gender#
phaseresult.phaseresult.custom_fields.event_number#
phaseresult.phaseresult.custom_fields.event_round#
phaseresult.phaseresult.official#
phaseresult.phaseresult.office.name'''.replace("#\n", ";")]

        styles = {'E': 'espalda', 'B': 'braza', 'M': 'mariposa',
                  'L': 'libre', 'S': 'estilos'}
        genders = {'F': 'female', 'M': 'male', 'X': 'mixed'}
        valid_events = ['50L', '100L', '200L', '400L', '800L', '1500L',
                        '2000L', '3000L', '50M', '100M', '200M', '50E', '100E',
                        '200E', '50B', '100B', '200B', '100S', '200S', '400S',
                        '4X50L', '4X100L', '4X200L', '4X50S', '4X100S',
                        '8X25S', '8X25L']

        sql_champ = '''
select champdesc, venuecode, season, lanes, poolleght, chronotype, champtype
from championship '''
        res = self.dbs.exec_sql(sql=sql_champ)
        (CHAMP_NAME, VENUE, SEASON_ID, LANES, POOL_ID, CHRONO_ID,
         ESTAMENT) = range(7)
        i = res[0]
        pool_id = i[POOL_ID] == '0' and "25" or "50"
        chrono_id = i[CHRONO_ID] == 'M' and "manual" or "electronic"
        estament = i[ESTAMENT] == 'M' and 'master' or 'deportista'
        champ = {'name': i[CHAMP_NAME], 'venue': i[VENUE],
                 'season_id': i[SEASON_ID], 'lanes': i[LANES],
                 'pool_id': pool_id, 'chrono_id': chrono_id,
                 'estament': estament}
        season_year_last = int('20{}'.format(champ['season_id'][3:]))

        sql_events = '''Select gendercode, eventcode, categorycode, xorder,
progressioncode from event'''
        res = self.dbs.exec_sql(sql=sql_events)
        (GENDER_ID, EVENT_ID, CATEGORY_ID, XORDER,
         PROGRESSION_ID) = range(5)
        events = {}
        for i in res:
            event_id = "{}.{}.{}".format(
                i[GENDER_ID], i[EVENT_ID], i[CATEGORY_ID])
            events[event_id] = {
                'order': int(i[XORDER]),
                'progression':  int(i[PROGRESSION_ID])}


        sql_event_splits = '''
select gendercode, eventcode, categorycode, splitnumber, distance,
recordeventcode from eventsplits '''
        res = self.dbs.exec_sql(sql=sql_event_splits)
        (GENDER_ID, EVENT_ID, CATEGORY_ID, SPLIT_NUMBER, DISTANCE,
         EVENT_ID_OF_SPLIT) = range(6)
        event_splits = {}
        for i in res:
            event_split_id = "{}.{}.{}.{}".format(
                i[GENDER_ID], i[EVENT_ID], i[CATEGORY_ID], i[SPLIT_NUMBER])
            if i[EVENT_ID_OF_SPLIT]:
                if i[EVENT_ID_OF_SPLIT].strip():
                    event_id_of_split = i[EVENT_ID_OF_SPLIT].strip()
                    valid = '1'
                else:
                    event_id_of_split = ''
                    valid = '0'
            else:
                event_id_of_split = ''
                valid = '0'
            event_splits[event_split_id] = {
                'distance': int(i[DISTANCE]),
                'event_id_of_split':  event_id_of_split,
                'valid':  valid}

        sql_champ_categories = '''
select categorycode, type, gendercode, fromyear, toyear from category '''
        res = self.dbs.exec_sql(sql=sql_champ_categories)
        (CATEGORY_ID, TYPE, GENDER_ID, FROM_YEAR, TO_YEAR) = range(5)
        champ_categories = {}
        for i in res:
            category_id = i[CATEGORY_ID].upper()
            if i[GENDER_ID] == 'F':
                category_id = '{}.F'.format(category_id)
            elif i[GENDER_ID] == 'M':
                category_id = '{}.M'.format(category_id)
            elif i[GENDER_ID] == 'X':
                category_id = '{}.X'.format(category_id)

            if category_id[0].isdigit() or category_id[0] == '+':
                category_id_lev = i[CATEGORY_ID].upper()
            else:
                category_id_lev = category_id

            if category_id_lev in cat_lev:
                name_lev = cat_lev[category_id_lev]
                min_age = str(season_year_last - int(i[TO_YEAR]))
                max_age = str(season_year_last - int(i[FROM_YEAR]))
            else:
                name_lev = 'Absoluto'
                min_age = '0'
                max_age = '99'

            champ_categories[category_id] = {'type': i[TYPE],
                                                 'gender_id': i[GENDER_ID],
                                                 'min_age': min_age,
                                                 'max_age': max_age,
                                                 'name_lev': name_lev}

        sql_phase = '''
select gendercode, eventcode, categorycode, phasecode, startdate, starttime
from phase '''

        res = self.dbs.exec_sql(sql=sql_phase)
        (GENDER_ID, EVENT_ID, CATEGORY_ID, PHASE_ID, DATE, TIME) = range(6)
        phases = {}
        for i in res:
            phase_id = "{}.{}.{}.{}".format(
                i[GENDER_ID], i[EVENT_ID], i[CATEGORY_ID], i[PHASE_ID])
            event_id = "{}.{}.{}".format(
                i[GENDER_ID], i[EVENT_ID], i[CATEGORY_ID])
            progression = events[event_id]['progression']
            if progression == 1:  # Final (contrareloxo)
                phase_name = 'TRIM'
            elif progression == 4:  # Preliminar - Final A B
                if i[PHASE_ID] == 2:
                    phase_name = 'PRE'
                elif i[PHASE_ID] == 1:
                    phase_name = 'FIN'
                else:
                    raise Exception('Non se atopou a fase')
            else:
                raise Exception('Non se atopou a progresión')

            phases[phase_id] = {
                'date_time': datetime2str(str2datetime('{} {}'.format(
                    i[DATE], i[TIME]))),
                'event_order': events[event_id]['order'],
                'phase_name': phase_name
                }

        sql_register = '''
select registerid, license, individualteam, familyname, givenname, gender,
clubcode, category from register '''
        res = self.dbs.exec_sql(sql=sql_register)
        (REGISTERID, LICENSE_ID, INDIVIDUALTEAM, FAMILYNAME, GIVENNAME, GENDER,
         CLUBCODE, CATEGORY_ID) = range(8)
        register = {}
        for i in res:
            lev_id = i[LICENSE_ID]
            # remove leading zeros
            while lev_id[0] == "0":
                lev_id = lev_id[1:]
            
            if lev_id in dni_levid_replaces:
                lev_id = dni_levid_replaces[lev_id]
                print(i[LICENSE_ID], lev_id)

            register[i[REGISTERID]] = {
                'lev_id': lev_id,
                'ind_rel': i[INDIVIDUALTEAM],
                'surname': i[FAMILYNAME],
                'name': i[GIVENNAME],
                'gender_id': i[GENDER],
                'club_id': i[CLUBCODE],
                'category_id': i[CATEGORY_ID]}

        sql_result_splits = '''
select gendercode, eventcode, categorycode, phasecode, registerid,
unitcode, lane, splitnumber, runningmark, member from resultsplits
        '''
        res = self.dbs.exec_sql(sql=sql_result_splits)
        (GENDER_ID, EVENT_ID, CATEGORY_ID, PHASE_ID, PERSON_ID,
         UNIT, LANE, SPLIT, MARK, MEMBER_ID) = range(10)

        res = [(i[GENDER_ID], i[EVENT_ID], i[CATEGORY_ID], i[PHASE_ID],
                i[PERSON_ID], i[UNIT], i[LANE], i[SPLIT],
                i[MARK].strip(), i[MEMBER_ID]) for i in res if i[MARK].strip()]
        res = sorted(res, key=lambda tup: (tup[0], tup[1], tup[2], tup[3],
                                           tup[4], tup[5], tup[6], tup[7]))
        result_splits = {}
        for i in res:
            result_split_id = "{}.{}.{}.{}.{}.{}.{}".format(
                i[GENDER_ID], i[EVENT_ID], i[CATEGORY_ID], i[PHASE_ID],
                i[PERSON_ID], i[UNIT], i[LANE])

            if i[MEMBER_ID] != '0':
                if result_split_id in result_splits:
                    result_splits[result_split_id].append(
                        {'split_number': i[SPLIT],
                        'mark': i[MARK].strip(),
                        'member_id': i[MEMBER_ID],
                        'category_id': i[CATEGORY_ID]})
                else:
                    result_splits[result_split_id] = [
                        {'split_number': i[SPLIT],
                        'mark': i[MARK].strip(),
                        'member_id': i[MEMBER_ID],
                        'category_id': i[CATEGORY_ID]}]

        sql_result = '''
SELECT gendercode, eventcode, categorycode, phasecode, registerid,
unitcode, lane, heattime, irmcode, irmsplitnumber from result '''
        res = self.dbs.exec_sql(sql=sql_result)
        (GENDER_ID, EVENT_ID, CATEGORY_ID, PHASE_ID, PERSON_ID,
         UNIT, LANE, MARK, ISSUE, ISSUE_SPLIT) = range(10)

        for i in res:
            if i[EVENT_ID] not in valid_events:
                print("Non valid event: ", i[EVENT_ID])
                continue
            if "X" in i[EVENT_ID]:
                members = int(i[EVENT_ID].split("X")[0])
                distance_relay = int(i[EVENT_ID].split("X")[1][:-1])
                distance_event = members * distance_relay
#                 distance = distance_current % distance_split
            else:
                members = 1
                distance_relay = int(i[EVENT_ID][:-1])
                distance_event = distance_relay

            result_split_id = "{}.{}.{}.{}.{}.{}.{}".format(
                i[GENDER_ID], i[EVENT_ID], i[CATEGORY_ID], i[PHASE_ID],
                i[PERSON_ID], i[UNIT], i[LANE])
            only_first_member = False
            if i[ISSUE].strip():
                if members == 1 or int(i[ISSUE_SPLIT]) == 1:
                    print("Resul with issue: ",
                          (register[i[PERSON_ID]]["surname"],
                           result_split_id,
                           'final',
                           i[ISSUE],
                           i[ISSUE_SPLIT]))
                    continue
                else:  # save first member
                    issue_distance = int(i[ISSUE_SPLIT]) * 50
                    if issue_distance > distance_relay:
                        only_first_member = True
                    else:
                        continue
            ind_rel = register[i[PERSON_ID]]["ind_rel"]
            style_id = i[EVENT_ID][-1]
            event_style = styles[style_id]
            phase_id = "{}.{}.{}.{}".format(
                i[GENDER_ID], i[EVENT_ID], i[CATEGORY_ID], i[PHASE_ID])
            date_time = phases[phase_id]['date_time']
            event_order = phases[phase_id]['event_order']
            phase_name = phases[phase_id]['phase_name']
            category_id = '{}.{}'.format(i[CATEGORY_ID], i[GENDER_ID])

            values_final = ()
            if not only_first_member:
                values_final = (
                    ind_rel == 'I' and register[i[PERSON_ID]]["lev_id"] or '',
                    ind_rel == 'I' and register[i[PERSON_ID]]["surname"] or '',
                    ind_rel == 'I' and register[i[PERSON_ID]]["name"] or '',
                    register[i[PERSON_ID]]["club_id"],
                    ind_rel == 'I' and 'license' or 'club',
                    event_style,
                    time2int(i[MARK].strip()),
                    distance_event,
                    members,
                    date_time,
                    champ_categories[category_id]['max_age'],
                    champ_categories[category_id]['min_age'],
                    champ_categories[category_id]['name_lev'],
                    str(38),
                    champ['name'],
                    champ['venue'],
                    champ['pool_id'],
                    champ['chrono_id'],
                    champ['venue'],
                    genders[i[GENDER_ID]],
                    str(event_order),
                    phase_name,
                    '1',
                    champ['estament']
                    )
                values_final_str = values_final.__str__()[1:-1]
                values_final_str = values_final_str.replace(", ", ";")
                values_final_str = values_final_str.replace("'", '"')

#             splits = []
            if result_split_id in result_splits:
                for j in result_splits[result_split_id]:
                    event_split_id = "{}.{}.{}.{}".format(
                        i[GENDER_ID], i[EVENT_ID], i[CATEGORY_ID],
                        j['split_number'])
                    event_split = event_splits[event_split_id]

#                     splits.append(j)
                    event_id_of_split = event_split['event_id_of_split']
                    if event_id_of_split:
                        split_style = styles[event_id_of_split[-1]]
                        valid = '1'
                    else:
                        split_style = event_style
                        valid = '0'
                    distance_current = event_split['distance']
                    if members > 1:
                        distance_split = distance_current % distance_relay
                        if distance_split == 0:
                            distance_split = distance_relay
                        if distance_current > distance_relay:
                            valid = '0'
                        category_id_member = '{}.{}'.format(j['category_id'],
                                                            i[GENDER_ID])
                    else:
                        distance_split = distance_current
                        category_id_member = category_id
                        if distance_current == distance_event:
                            continue  # for individual event, skip last split
                    
                    values_split = (
                        register[j['member_id']]["lev_id"],
                        register[j['member_id']]["surname"],
                        register[j['member_id']]["name"],
                        register[j['member_id']]["club_id"],
                        'license',
                        split_style,
                        time2int(j['mark'].strip()),
                        distance_split,
                        1,
                        date_time,
                        champ_categories[category_id_member]['max_age'],
                        champ_categories[category_id_member]['min_age'],
                        champ_categories[category_id_member]['name_lev'],
                        str(38),
                        champ['name'],
                        champ['venue'],
                        champ['pool_id'],
                        champ['chrono_id'],
                        champ['venue'],
                        genders[register[j['member_id']]["gender_id"]],
                        str(event_order),
                        phase_name,
                        valid,
                        champ['estament']
                        )
                    values_split_str = values_split.__str__()[1:-1]
                    values_split_str = values_split_str.replace(", ", ";")
                    values_split_str = values_split_str.replace("'", '"')
                    if not only_first_member or (only_first_member and valid == '1'):
                        if values_final: # add parent fields
                            values_split_str += "," + values_final_str
                        lines.append(values_split_str)
                    # print(values_split_str)

            lines.append(values_final_str)
            # print(values_final_str)


#         lines = reversed(lines)
#         lines = "\n".join(lines)
#         lines += "\n"

        file_path = '{}_{}.csv'.format(self.file_path, champ['estament'][:5])

        set_file_content(
            content=lines,
            file_path=file_path,
            compress=False,
            binary=False,
            encoding='utf-8',
            end_line="\n")


