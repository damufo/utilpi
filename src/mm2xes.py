# -*- coding: utf-8 -*-


# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Copyright (C) 2019 Federacion Galega de Natación (FEGAN) http://www.fegan.org
# Author: Daniel Muñiz Fontoira (2017) <dani@damufo.com>


import os
from mm2xes.mm2xes import Mm2Xes

if os.name == 'posix':
    platform = "lin"
else:
    platform = "win"
app_path_folder = os.path.dirname(os.path.realpath(__file__))
dbs_path = None
list_files = os.listdir(app_path_folder)
for i in list_files:
    if os.path.splitext(i)[1] == '.mdb':
        dbs_path = os.path.join(app_path_folder, i)
        file_path = "{}.insc".format(dbs_path[:-4]) 
        break

if dbs_path:
    mm2xes = Mm2Xes(platform=platform, dbs_path=dbs_path)
    mm2xes.gen_insc(file_path=file_path)
    mm2xes.dbs.close()

